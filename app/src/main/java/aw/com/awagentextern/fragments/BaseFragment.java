package aw.com.awagentextern.fragments;


import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import aw.com.awagentextern.util.HeaderService;

public abstract class BaseFragment extends Fragment implements HeaderService {

	@Override
	public void setHeaderTitle(Activity activity, int title) {
		((AppCompatActivity) activity).getSupportActionBar().setTitle(title);
	}

	@Override
	public void setHeaderTitle(Activity activity, String title) {
		((AppCompatActivity) activity).getSupportActionBar().setTitle(title);
	}

	/**
	 * Hide onScreen keyboard.
	 *
	 * @param context Context
	 */
	public static void hideKeyboard(Context context) {
		InputMethodManager inputManager = (InputMethodManager) context
			.getSystemService(Context.INPUT_METHOD_SERVICE);

		// check if no view has focus:
		View view = ((Activity) context).getCurrentFocus();
		if (view == null) {
			return;
		}

		inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
}
