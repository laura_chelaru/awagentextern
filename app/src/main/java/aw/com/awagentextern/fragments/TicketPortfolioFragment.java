package aw.com.awagentextern.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.List;

import aw.com.awagentextern.R;
import aw.com.awagentextern.api.ApiClient;
import aw.com.awagentextern.api.DefaultApiCallback;
import aw.com.awagentextern.api.models.Ticket;
import aw.com.awagentextern.api.services.TicketPortfolioService;
import aw.com.awagentextern.items.TicketPortfolioItem;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Retrofit;

public class TicketPortfolioFragment extends BaseFragment implements View.OnClickListener, TicketAddFragment.OnSubmitListener{

	@BindView(R.id.ticket_list_no_items)
	protected TextView ticketListNoItems;

	@BindView(R.id.tickets_loading_panel)
	protected ProgressBar ticketsLoadingPanel;

	@BindView(R.id.tickets_list)
	protected RecyclerView ticketsList;

	@BindView(R.id.ticket_add_button)
	protected FloatingActionButton ticketAddButton;

	protected FastItemAdapter<TicketPortfolioItem> fastItemAdapter = new FastItemAdapter<>();
	protected List<TicketPortfolioItem> ticketSearchList = new ArrayList<>();;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_main_ticket_portfolio_fragment, container, false);

		ButterKnife.bind(this, view);

		ticketAddButton.setOnClickListener(this);

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		final Activity activity = getActivity();

		setHeaderTitle(activity, R.string.ticket_list);

		LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
		ticketsList.setLayoutManager(layoutManager);
		ticketsList.setAdapter(fastItemAdapter);

		putTicketList();
	}

	protected void putTicketList() {
		ticketListNoItems.setVisibility(View.GONE);
		ticketsLoadingPanel.setVisibility(View.VISIBLE);

		Retrofit retrofit = ApiClient.getInstance(getActivity()).getRetrofit();
		TicketPortfolioService ticketPortfolioService = retrofit.create(TicketPortfolioService.class);

		Call<List<Ticket>> call = ticketPortfolioService.getTickets();

		call.enqueue(new DefaultApiCallback<List<Ticket>>(getActivity()) {
			@Override
			public void onResponseSuccess(List<Ticket> responseBody, Call<List<Ticket>> call, Headers headers) {
				ticketSearchList.clear();
				if (responseBody.isEmpty()) {
					ticketListNoItems.setVisibility(View.VISIBLE);
					ticketsLoadingPanel.setVisibility(View.GONE);
				} else {
					for (Ticket ticket: responseBody) {
						ticketSearchList.add(new TicketPortfolioItem(ticket));
					}
				}

				fastItemAdapter.setNewList(ticketSearchList);
				ticketsLoadingPanel.setVisibility(View.GONE);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.ticket_add_button:

				TicketAddFragment ticketAddFragment =
					TicketAddFragment.newInstance();

				ticketAddFragment.setOnSubmitListener(this);
				ticketAddFragment.show(getActivity().getFragmentManager(), null);

				break;
		}
	}

	@Override
	public void submitNewTicket(Ticket newTicket) {
		final Activity activity = getActivity();

		Retrofit retrofit = ApiClient.getInstance(activity).getRetrofit();
		TicketPortfolioService ticketPortfolioService = retrofit.create(TicketPortfolioService.class);
		Call<Void> call = ticketPortfolioService.postTicket(newTicket);

		call.enqueue(new DefaultApiCallback<Void>(activity) {

			@Override
			public void onResponseSuccess(Void responseBody, Call<Void> call, Headers headers) {
				putTicketList();
			}
		});
	}
}
