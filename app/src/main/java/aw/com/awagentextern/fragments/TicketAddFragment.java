package aw.com.awagentextern.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import aw.com.awagentextern.R;
import aw.com.awagentextern.api.ApiClient;
import aw.com.awagentextern.api.DefaultApiCallback;
import aw.com.awagentextern.api.models.Ticket;
import aw.com.awagentextern.api.services.TicketPortfolioService;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Retrofit;

public class TicketAddFragment extends DialogFragment{

	@BindView(R.id.input_layout_ticket_title)
	protected TextInputLayout inputLayoutTicketTitle;

	@BindView(R.id.input_ticket_title)
	protected EditText inputTicketTitle;

	@BindView(R.id.input_layout_ticket_description)
	protected TextInputLayout inputLayoutTicketDescription;

	@BindView(R.id.input_ticket_description)
	protected EditText inputTicketDescription;

	private TicketAddFragment.OnSubmitListener submitListener;

	public interface OnSubmitListener {
		void submitNewTicket(Ticket newTicket);
	}


	public void setOnSubmitListener(TicketAddFragment.OnSubmitListener submitListener) {
		this.submitListener = submitListener;
	}

	/**
	 * Instantiate the ApartmentFilterDialogFragment.
	 *
	 * @return EventCancelDialogFragment
	 */
	public static TicketAddFragment newInstance() {
		Bundle args = new Bundle();

		TicketAddFragment fragment = new TicketAddFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View rootView = inflater.inflate(R.layout.activity_main_ticket_add_fragment, null, false);

		ButterKnife.bind(this, rootView);

		return new AlertDialog.Builder(getActivity())
			.setView(rootView)
			.setPositiveButton(R.string.finalize,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {

					}
				}
			)
			.setNegativeButton(R.string.back,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
					}
				}
			)
			.create();
	}

	@Override
	public void onStart() {
		super.onStart();
		AlertDialog dialog = (AlertDialog)getDialog();
		if (dialog != null) {
			Button positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
			positiveButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Boolean wantToCloseDialog = false;

					String title = inputTicketTitle.getText().toString();
					String description = inputTicketDescription.getText().toString();
					boolean isError = false;

					if (title.isEmpty()) {
						inputLayoutTicketTitle.setError(getActivity().getResources().getString(R.string.error_empty));
						isError = true;
					}

					if(description.isEmpty()) {
						inputLayoutTicketDescription.setError(getActivity().getResources().getString(R.string.error_empty));
						isError = true;
					}

					if (isError) {
						return;
					} else {
						Ticket newTicket = new Ticket();

						newTicket.setTitle(title);
						newTicket.setDescription(description);

						wantToCloseDialog = true;

						if (submitListener != null) {
							submitListener.submitNewTicket(newTicket);
						}
					}

					if (wantToCloseDialog) {
						dismiss();
					}
				}
			});
		}
	}



}
