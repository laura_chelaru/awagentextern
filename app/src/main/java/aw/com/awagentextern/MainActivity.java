package aw.com.awagentextern;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import aw.com.awagentextern.api.ApiClient;
import aw.com.awagentextern.api.models.AgentProfile;
import aw.com.awagentextern.api.models.LoginResponse;
import aw.com.awagentextern.fragments.TicketPortfolioFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener,
	Drawer.OnDrawerItemClickListener {

	protected static final int ID_TICKET_LIST = 1;
	protected static final int ID_LOGOUT = 2;

	@BindView(R.id.toolbar)
	protected Toolbar toolbar;

	protected AccountHeader headerResult = null;
	protected Drawer result = null;
	protected LoginResponse agent;
	protected ActionBarDrawerToggle mDrawerToggle;
	protected boolean mToolBarNavigationListenerIsRegistered = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ButterKnife.bind(this);

		toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportFragmentManager().addOnBackStackChangedListener(this);

		Bundle loginBundle = this.getIntent().getExtras();
		if (loginBundle != null) {
			agent = (LoginResponse) loginBundle.getSerializable("agent");
		} else {
			return;
		}

		PrimaryDrawerItem ticketList = new PrimaryDrawerItem()
			.withIdentifier(ID_TICKET_LIST)
			.withName(R.string.ticket_list)
			.withSelectable(true);
		PrimaryDrawerItem logout = new PrimaryDrawerItem()
			.withIdentifier(ID_LOGOUT)
			.withName(R.string.logout)
			.withSelectable(true);

		ProfileDrawerItem profileDrawerItem = new ProfileDrawerItem();
		profileDrawerItem.withIdentifier((long) agent.getData().getId())
			.withName(agent.getData().getFirstName() + " " + agent.getData().getLastName())
			.withEmail(agent.getData().getEmail());

		headerResult = new AccountHeaderBuilder()
			.withActivity(this)
			.withHeaderBackground(R.color.colorPrimary)
			.addProfiles(profileDrawerItem)
			.withSelectionListEnabledForSingleProfile(false)
			.build();

		result = new DrawerBuilder()
			.withActivity(this)
			.withToolbar(toolbar)
			.withAccountHeader(headerResult)
			.addDrawerItems(
				ticketList,
				logout
			)
			.withSelectedItem(1)
			.withOnDrawerItemClickListener(this)
			.withDisplayBelowStatusBar(false)
			.withTranslucentStatusBar(false)
			.build();

		mDrawerToggle = new ActionBarDrawerToggle(this, result.getDrawerLayout(), toolbar, R.string.open, R.string.close);
		result.getDrawerLayout().addDrawerListener(mDrawerToggle);
		mDrawerToggle.syncState();
		shouldDisplayHomeUp();

		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragment, new TicketPortfolioFragment());
		result.setSelection(ID_TICKET_LIST);

		fragmentTransaction.commit();
	}

	public void shouldDisplayHomeUp(){
		//Enable Up button only  if there are entries in the back stack
		boolean canback = getSupportFragmentManager().getBackStackEntryCount()>0;
		if(canback) {
			// Remove hamburger
			mDrawerToggle.setDrawerIndicatorEnabled(false);
			// Show back button
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			// when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
			// clicks are disabled i.e. the UP button will not work.
			// We need to add a listener, as in below, so DrawerToggle will forward
			// click events to this listener.
			if(!mToolBarNavigationListenerIsRegistered) {
				mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						// Doesn't have to be onBackPressed
						onBackPressed();
					}
				});

				mToolBarNavigationListenerIsRegistered = true;
			}

		} else {
			// Remove back button
			getSupportActionBar().setDisplayHomeAsUpEnabled(false);
			// Show hamburger
			mDrawerToggle.setDrawerIndicatorEnabled(true);
			// Remove the/any drawer toggle listener
			mDrawerToggle.setToolbarNavigationClickListener(null);
			mToolBarNavigationListenerIsRegistered = false;
		}

	}

	@Override
	public void onBackStackChanged() {

	}

	@Override
	public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
		if (drawerItem != null) {
			FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

			int id = (int) drawerItem.getIdentifier();
			switch(id) {
				case ID_TICKET_LIST:
					fragmentTransaction.replace(R.id.fragment, new TicketPortfolioFragment());
					break;
				case ID_LOGOUT:
					ApiClient.logoutAgent(MainActivity.this);
					break;
				default:
					break;
			}
			if (fragmentTransaction.commit() > 0) {
				return true;
			}
		}



		return false;
	}
}
