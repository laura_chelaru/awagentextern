package aw.com.awagentextern.api.services;

import java.util.List;

import aw.com.awagentextern.api.models.Ticket;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface TicketPortfolioService {

	@GET("v1/external_agent/tickets")
	Call<List<Ticket>> getTickets();

	@POST("v1/external_agent/tickets")
	Call<Void> postTicket(@Body Ticket ticket);
}
