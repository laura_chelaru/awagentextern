package aw.com.awagentextern.api;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;

import com.google.gson.JsonSyntaxException;


import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class ApiCallback<T> implements Callback<T> {
	public static final int HTTP_CODE_401 = 401;
	public static final int HTTP_CODE_403 = 403;
	public static final int HTTP_CODE_422 = 422;

	private Context context;

	ApiCallback(Context context) {
		this.context = context;
	}

	public abstract void onResponseSuccess(T responseBody, Call<T> call, Headers headers);

	public boolean onAccessDenied() {
		return false;
	}

	public void onResponseFailure(Response<T> response, Call<T> call ) {}

	public void onAnyFailure(Call<T> call) {}

	private void onFinish() {}

	public void onJsonParseFailure(Call<T> call, Throwable t) {}

	public boolean onNoNetwork(Call<T> call, Throwable t) {
		return false;
	}

	public void onNetworkFailure(Call<T> call, Throwable t) {}

	public boolean onNoInternet(Call<T> call, Throwable t) {
		return false;
	}

	@Override
	public void onResponse(Call<T> call, Response<T> response) {
		if (response.isSuccessful()) {
			onResponseSuccess(response.body(), call, response.headers());
		} else {
			if (response.code() == HTTP_CODE_401) {
				if (onAccessDenied()) {
					onResponseFailure(response, call);
				}
			} else {
				onResponseFailure(response, call);
			}

			onAnyFailure(call);
		}

		onFinish();
	}

	@Override
	public void onFailure(final Call<T> call, final Throwable t) {
		if (t instanceof JsonSyntaxException) {
			onJsonParseFailure(call, t);
		} else {
			if (!NetworkManager.isConnected(context)) {
				if(!onNoNetwork(call, t)) {
					onNetworkFailure(call, t);
				}
			} else {
				final Handler mainHandler = new Handler(context.getMainLooper());

				NetworkManager.checkInternetAvailable(context, new InternetConnectionCallback() {
					@Override
					public void internetConnectionUnavailable() {
						Runnable myRunnable = new Runnable() {
							@Override
							public void run() {
								if (!onNoInternet(call, t)) {
									onNetworkFailure(call, t);
								}
							}
						};
						mainHandler.post(myRunnable);
					}

					@Override
					public void internetConnectionAvailable() {
						Runnable myRunnable = new Runnable() {
							@Override
							public void run() {
								onNetworkFailure(call, t);
							}
						};

						mainHandler.post(myRunnable);
					}
				});
			}
		}
	}
}
