package aw.com.awagentextern.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponse implements Serializable{

	@SerializedName("success")
	@Expose
	protected boolean success;

	@SerializedName("data")
	@Expose
	protected AgentProfile data;

	/**
	 * Request success status.
	 *
	 * @return boolean
	 */
	public boolean getSuccess() {
		return success;
	}

	/**
	 * Request success status.
	 *
	 * @param success boolean
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * Response data.
	 *
	 * @return AgentProfile
	 */
	public AgentProfile getData() {
		return data;
	}

	/**
	 * Response data.
	 *
	 * @param data AgentProfile
	 */
	public void setData(AgentProfile data) {
		this.data = data;
	}
}
