package aw.com.awagentextern.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Ticket implements Serializable {

	@SerializedName("title")
	@Expose
	protected String title;

	@SerializedName("description")
	@Expose
	protected String description;

	/**
	 * Ticket title.
	 *
	 * @return String
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Ticket title.
	 *
	 * @param title String
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Ticket description.
	 *
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Ticket description.
	 *
	 * @param description String
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
