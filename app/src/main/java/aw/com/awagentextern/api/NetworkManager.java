package aw.com.awagentextern.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class NetworkManager {
	private static final String CONNECTION_CHECK_URL = "http://clients1.google.com/generate_204";

	public static boolean isConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnected();
	}

	public static void checkInternetAvailable(Context context, final InternetConnectionCallback listener) {
		if(isConnected(context)) {
			listener.internetConnectionUnavailable();
		} else {
			ApiClient apiClient = ApiClient.getInstance(context);

			Request request = new Request.Builder()
				.url(CONNECTION_CHECK_URL)
				.build();

			OkHttpClient httpClient = apiClient.getClient();

			httpClient.newCall(request).enqueue(new Callback() {

				@Override
				public void onFailure(Call call, IOException e) {
					listener.internetConnectionUnavailable();
				}

				@Override
				public void onResponse(Call call, Response response) throws IOException {
					if (response.code() != 204) {
						listener.internetConnectionUnavailable();
					} else {
						listener.internetConnectionAvailable();
					}
				}
			});
		}
	}
}
