package aw.com.awagentextern.api.services;

import aw.com.awagentextern.api.models.AgentProfile;
import aw.com.awagentextern.api.models.LoginRequest;
import aw.com.awagentextern.api.models.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface LoginService {

	@POST("auth/external_agent/sign_in")
	Call<LoginResponse> signIn(@Body LoginRequest data);

	@GET("auth/external_agent/validate_token")
	Call<LoginResponse> validateToken();

	@GET("auth/external_agent/sign_out")
	Call<Void> signOut();
}
