package aw.com.awagentextern.api;

import android.content.Context;

import java.io.IOException;

import aw.com.awagentextern.util.Storage;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class AuthenticationInterceptor implements Interceptor {

	private static final String ACCESS_TOKEN_KEY = "access-token";
	private static final String CLIENT_KEY = "client";
	private static final String EXPIRY_KEY = "expiry";
	private static final String UID_KEY = "uid";
	private Context context;

	AuthenticationInterceptor(Context context) {
		this.context = context;
	}

	@Override
	public Response intercept(Interceptor.Chain chain) throws IOException {
		Request.Builder requestBuilder = chain.request().newBuilder();

		String accessToken = Storage.getAccessToken(context);
		String client = Storage.getClient(context);
		String expiry = Storage.getExpiry(context);
		String uid = Storage.getUid(context);

		if (accessToken != null) {
			requestBuilder.addHeader(ACCESS_TOKEN_KEY, accessToken);
		}

		if (client != null) {
			requestBuilder.addHeader(CLIENT_KEY, client);
		}

		if (expiry != null) {
			requestBuilder.addHeader(EXPIRY_KEY, expiry);
		}

		if (uid != null) {
			requestBuilder.addHeader(UID_KEY, uid);
		}

		Request request = requestBuilder
			.addHeader("Content-Type", "application/json")
			.addHeader("Accept", "application/json")
			.build();

		return chain.proceed(request);
	}
}
