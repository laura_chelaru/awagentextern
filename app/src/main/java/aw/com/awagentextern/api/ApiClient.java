package aw.com.awagentextern.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import aw.com.awagentextern.LoginActivity;
import aw.com.awagentextern.util.Storage;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
	private static final String BASE_URL_KEY = "aw.com.awagentextern.BASE_URL";
	private static ApiClient instance;
	private Retrofit retrofit;
	private OkHttpClient client;
	private Gson gson;

	private ApiClient(Context context) {
		HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
		loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

		AuthenticationInterceptor authenticationInterceptor = new AuthenticationInterceptor(context);

		String baseUrl;

		try {
			ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
			Bundle bundle = ai.metaData;
			baseUrl = bundle.getString(BASE_URL_KEY);
		} catch (PackageManager.NameNotFoundException e) {
			throw new RuntimeException(e);
		}

		gson = new GsonBuilder()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
			.setLenient()
			.create();

		client = new OkHttpClient.Builder()
			.addInterceptor(authenticationInterceptor)
			.addInterceptor(loggingInterceptor)
			.build();

		retrofit = new Retrofit.Builder()
			.baseUrl(baseUrl)
			.addConverterFactory(GsonConverterFactory.create(gson))
			.client(client)
			.build();
	}

	public static synchronized ApiClient getInstance(Context context) {
		if (instance == null) {
			instance = new ApiClient(context);
		}

		return instance;
	}

	public Retrofit getRetrofit() {
		return retrofit;
	}

	public OkHttpClient getClient() {
		return client;
	}

	public Gson getGson() {
		return gson;
	}

	public static void logoutAgent(Activity activity) {
		Storage.setAccessToken(activity, null);
		Storage.setClient(activity, null);
		Storage.setExpiry(activity, null);
		Storage.setUid(activity, null);
		Intent loginIntent = new Intent(activity, LoginActivity.class);
		loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		activity.startActivity(loginIntent);
	}
}
