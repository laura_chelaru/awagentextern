package aw.com.awagentextern.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginRequest implements Serializable {

	@SerializedName("email")
	@Expose
	private String email;

	@SerializedName("password")
	@Expose
	private String password;

	/**
	 * User email.
	 *
	 * @return String
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * User email.
	 *
	 * @param email String
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * User password.
	 *
	 * @return String
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * User password.
	 *
	 * @param password String
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
