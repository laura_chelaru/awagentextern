package aw.com.awagentextern.api;

import java.io.IOException;
import java.lang.annotation.Annotation;

import aw.com.awagentextern.api.models.AgentProfile;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

	public static AgentProfile parseErrorAgentProfile(Response<?> response) {
		Converter<ResponseBody, AgentProfile> converter =
			ServiceGenerator.retrofit
				.responseBodyConverter(AgentProfile.class, new Annotation[0]);

		AgentProfile error;

		try{
			error = converter.convert(response.errorBody());
		} catch (IOException e) {
			return new AgentProfile();
		}

		return error;
	}
}
