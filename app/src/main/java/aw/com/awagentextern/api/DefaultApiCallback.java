package aw.com.awagentextern.api;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import aw.com.awagentextern.R;
import retrofit2.Call;
import retrofit2.Response;

public abstract class DefaultApiCallback<T> extends ApiCallback<T> {

	private Activity activity;

	public DefaultApiCallback(Activity activity) {
		super(activity.getApplicationContext());

		this.activity = activity;
	}

	@Override
	public  void onResponseFailure(Response<T> response, Call<T> call) {
		switch(response.code()) {
			case HTTP_CODE_422:
				Toast.makeText(activity, activity.getString(R.string.error_unprocessable_entity), Toast.LENGTH_LONG).show();
				break;
			default:
				Toast.makeText(activity, activity.getString(R.string.error_response), Toast.LENGTH_SHORT).show();
		}
	}

	public void onJsonParseFailure(Call<T> call, Throwable t) {
		Toast.makeText(activity, activity.getString(R.string.error_invalid_json), Toast.LENGTH_SHORT).show();
		//Log.d("Invalid sever answer", t.getMessage);
	}

	@Override
	public void onNetworkFailure(final Call<T> call, final Throwable t) {
		Toast.makeText(activity, activity.getString(R.string.error_network_failure), Toast.LENGTH_SHORT).show();
		//Log.d("Network Error", t.getMessage());
	}

	@Override
	public boolean onNoNetwork(Call<T> call, Throwable t) {
		Toast.makeText(activity, activity.getString(R.string.error_no_network), Toast.LENGTH_SHORT).show();
		return true;
	}

	@Override
	public boolean onNoInternet(Call<T> call, Throwable t) {
		Toast.makeText(activity, activity.getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
		//Log.d("Internet Error", t.getMessage());
		return true;
	}

	@Override
	public boolean onAccessDenied() {
		ApiClient.logoutAgent(activity);
		return true;
	}


}
