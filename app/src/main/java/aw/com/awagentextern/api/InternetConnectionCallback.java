package aw.com.awagentextern.api;

public interface InternetConnectionCallback {
	void internetConnectionUnavailable();

	void internetConnectionAvailable();
}
