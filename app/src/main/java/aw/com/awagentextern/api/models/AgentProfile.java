package aw.com.awagentextern.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AgentProfile implements Serializable{

	@SerializedName("id")
	@Expose
	private Integer id;

	@SerializedName("first_name")
	@Expose
	private String firstName;

	@SerializedName("last_name")
	@Expose
	private String lastName;

	@SerializedName("email")
	@Expose
	private String email;

	@SerializedName("role")
	@Expose
	private String role;

	@SerializedName("active")
	@Expose
	private Boolean active;

	@SerializedName("errors")
	@Expose
	private List<String> errors;

	/**
	 * Client id.
	 *
	 * @return Integer
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Client id.
	 *
	 * @param id Integer
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * User first name.
	 *
	 * @return String
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * User first name.
	 *
	 * @param firstName String
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * User last name.
	 *
	 * @return String
	 */
	public String getLastName() {
		return  lastName;
	}

	/**
	 * User last name.
	 *
	 * @param lastName String
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * User email.
	 *
	 * @return String
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * User email.
	 *
	 * @param email String
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * User role.
	 *
	 * @return String
	 */
	public String getRole() {
		return role;
	}

	/**
	 * User role.
	 *
	 * @param role String
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * User active flag.
	 *
	 * @return Boolean
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * User active flag.
	 *
	 * @param active Boolean
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * Errors on response.
	 *
	 * @return List
	 */
	public List<String> getErrors() {
		return errors;
	}

	/**
	 * Errors on response.
	 *
	 * @param errors List
	 */
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}
