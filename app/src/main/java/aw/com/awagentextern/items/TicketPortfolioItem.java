package aw.com.awagentextern.items;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import aw.com.awagentextern.R;
import aw.com.awagentextern.api.models.Ticket;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TicketPortfolioItem extends AbstractItem<TicketPortfolioItem, TicketPortfolioItem.ViewHolder> {

	private Ticket ticket;

	public TicketPortfolioItem(Ticket ticket) {
		this.ticket = ticket;
	}

	public Ticket getTicket() {
		return ticket;
	}

	@Override
	public void bindView(ViewHolder viewHolder, List<Object> payloads) {
		super.bindView(viewHolder, payloads);

		viewHolder.ticketTitle.setText(ticket.getTitle());
		viewHolder.ticketDescription.setText(ticket.getDescription());
	}

	@NonNull
	@Override
	public ViewHolder getViewHolder(View v) {
		return new ViewHolder(v);
	}

	@Override
	public int getType() {
		return R.id.ticket_portfolio_item;
	}

	@Override
	public int getLayoutRes() {
		return R.layout.item_ticket_portfolio;
	}

	static class ViewHolder extends RecyclerView.ViewHolder {
		@BindView(R.id.ticket_title)
		protected TextView ticketTitle;

		@BindView(R.id.ticket_description)
		protected TextView ticketDescription;

		public ViewHolder(View itemView) {
			super(itemView);

			ButterKnife.bind(this, itemView);
		}
	}
}
