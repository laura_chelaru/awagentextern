package aw.com.awagentextern;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import aw.com.awagentextern.api.ApiClient;
import aw.com.awagentextern.api.DefaultApiCallback;
import aw.com.awagentextern.api.ErrorUtils;
import aw.com.awagentextern.api.models.AgentProfile;
import aw.com.awagentextern.api.models.LoginRequest;
import aw.com.awagentextern.api.models.LoginResponse;
import aw.com.awagentextern.api.services.LoginService;
import aw.com.awagentextern.util.Storage;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

	@BindView(R.id.login_form)
	protected LinearLayout loginForm;

	@BindView(R.id.login_error_message)
	protected TextView loginErrorMessage;

	@BindView(R.id.input_layout_email)
	protected TextInputLayout inputLayoutEmail;

	@BindView(R.id.input_email)
	protected EditText inputEmail;

	@BindView(R.id.input_layout_password)
	protected TextInputLayout inputLayoutPassword;

	@BindView(R.id.input_password)
	protected EditText inputPassword;

	@BindView(R.id.login_button)
	protected Button loginButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		setContentView(R.layout.activity_login);

		ButterKnife.bind(this);

		loginButton.setOnClickListener(this);

		if (Storage.getAccessToken(this) != null) {
			checkLoginToken();
		} else {
			visibleLogin();
		}
	}

	protected void checkLoginToken() {
		Retrofit retrofit = ApiClient.getInstance(this).getRetrofit();
		LoginService loginService = retrofit.create(LoginService.class);
		Call<LoginResponse> call = loginService.validateToken();

		call.enqueue(new DefaultApiCallback<LoginResponse>(this) {
			@Override
			public void onResponseSuccess(LoginResponse responseBody, Call<LoginResponse> call, Headers headers) {
				startMainActivity(responseBody);
			}

			@Override
			public void onAnyFailure(Call<LoginResponse> call) {
				visibleLogin();
			}

			@Override
			public boolean onAccessDenied() {
				return true;
			}
		});
	}

	protected void visibleLogin() {
		loginForm.setVisibility(View.VISIBLE);

		String lastUsername = Storage.getLastUsername(this);
		inputEmail.setText(lastUsername);

		if (lastUsername != null) {
			inputPassword.requestFocus();
		}
	}

	protected void login() {
		loginErrorMessage.setVisibility(View.GONE);
		inputLayoutEmail.setError(null);
		inputLayoutPassword.setError(null);

		String email = inputEmail.getText().toString();
		String password = inputPassword.getText().toString();

		boolean inputError = false;

		if (email.isEmpty()) {
			inputLayoutEmail.setError(getString(R.string.error_empty));
			inputError = true;
		} else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
			inputLayoutEmail.setError(getString(R.string.error_email));
			inputError = true;
		}

		if (password.isEmpty()) {
			inputLayoutPassword.setError(getString(R.string.error_empty));
			inputError = true;
		}

		if (inputError) {
			return;
		}

		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setEmail(email);
		loginRequest.setPassword(password);

		final Retrofit retrofit = ApiClient.getInstance(this).getRetrofit();
		LoginService loginService = retrofit.create(LoginService.class);
		Call<LoginResponse> call = loginService.signIn(loginRequest);

		Storage.setLastUsername(this, loginRequest.getEmail());

		call.enqueue(new DefaultApiCallback<LoginResponse>(this) {
			@Override
			public void onResponseSuccess(LoginResponse responseBody, Call<LoginResponse> call, Headers headers) {
				Context context = getApplicationContext();

				Storage.setAccessToken(context, headers.get("access-token"));
				Storage.setClient(context, headers.get("client"));
				Storage.setExpiry(context, headers.get("expiry"));
				Storage.setUid(context, headers.get("uid"));

				startMainActivity(responseBody);
			}

			@Override
			public boolean onAccessDenied() {
				return true;
			}

			@Override
			public void onResponseFailure(Response<LoginResponse> response, Call<LoginResponse> call) {
				AgentProfile error = ErrorUtils.parseErrorAgentProfile(response);

				loginErrorMessage.setVisibility(View.VISIBLE);
				loginErrorMessage.setText(error.getErrors().get(0));
			}
		});
	}

	protected void startMainActivity(LoginResponse loginResponse) {
		Intent mainIntent = new Intent(this, MainActivity.class);
		mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		mainIntent.putExtra("agent", loginResponse);

		String action = getIntent().getAction();
		if (action != null) {
			mainIntent.setAction(action);
		}

		startActivity(mainIntent);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.login_button:
				login();
				break;
			default:
				break;
		}
	}
}
