package aw.com.awagentextern.util;

import android.app.Activity;

public interface HeaderService {
	void setHeaderTitle(Activity activity, int title);

	void setHeaderTitle(Activity activity, String title);
}
