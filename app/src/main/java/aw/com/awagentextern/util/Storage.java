package aw.com.awagentextern.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class Storage {

	private static final String PREF_NAME = "aw_pref";
	private static final String ACCESS_TOKEN_KEY = "access-token";
	private static final String CLIENT_KEY = "client";
	private static final String EXPIRY_KEY = "expiry";
	private static final String UID_KEY = "uid";
	private static final String LAST_USERNAME_KEY = "last_username";

	private static String getString(Context context, String key, String defaultValue) {
		SharedPreferences sharedPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		return sharedPref.getString(key, defaultValue);
	}

	@SuppressLint("ApplySharedPref")
	private static void setString (Context context, String  key, String value) {
		SharedPreferences sharedPref = context.getSharedPreferences(PREF_NAME, context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static String getAccessToken(Context context) {
		return getString(context, ACCESS_TOKEN_KEY, null);
	}

	public static void setAccessToken(Context context, String value) {
		setString(context, ACCESS_TOKEN_KEY, value);
	}

	public static String getClient(Context context) {
		return getString(context, CLIENT_KEY, null);
	}

	public static void setClient(Context context, String value) {
		setString(context, CLIENT_KEY, value);
	}

	public static String getExpiry(Context context) {
		return getString(context, EXPIRY_KEY, null);
	}

	public static void setExpiry(Context context, String value) {
		setString(context, EXPIRY_KEY, value);
	}

	public static String getUid(Context context) {
		return getString(context, UID_KEY, null);
	}

	public static void setUid(Context context, String value) {
		setString(context, UID_KEY, value);
	}

	public static String getLastUsername(Context context) {
		return getString(context, LAST_USERNAME_KEY, null);
	}

	public static void setLastUsername(Context context, String value) {
		setString(context, LAST_USERNAME_KEY, value);
	}
}
